/*
 * Public API Surface of lib2
 */
import { Lib1Service } from '@Test/lib1';

export * from './lib/lib2.service';
export * from './lib/lib2.component';
