import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ButtonLoaderService {
  //   state: 'initial' | 'loading' | 'success' | 'error' = 'initial';
  state = new BehaviorSubject('initial');
  state$ = this.state.asObservable();

  getState() {
    return this.state.getValue();
  }

  setState(state: 'initial' | 'loading' | 'success' | 'error') {
    this.state.next(state);
    if (state === 'success' || state === 'error') {
      setTimeout(() => {
        this.state.next('initial');
      }, 2000);
    }
  }
}
