// import { Component } from '@angular/core';
// import { ButtonLoaderService } from '../button-loader.service';

// @Component({
//   selector: 'app-button',
//   standalone: true,
//   template: `
//     <button [disabled]="loader.state === 'loading'">Button Component</button>
//   `,
//   providers: [ButtonLoaderService],
// })
// export class ButtonComponent {
//   constructor(protected loader: ButtonLoaderService) {
//     console.log(this.loader.state);
//   }
// }

import { Component, Input } from '@angular/core';
import { ButtonLoaderService } from '../button-loader.service';

@Component({
  selector: 'app-button',
  standalone: true,
  template: `
    <button [disabled]="state === 'loading'">
      {{ state }} Button Component
    </button>
  `,
  providers: [ButtonLoaderService],
})
export class ButtonComponent {
  @Input('state') state = 'initial';
}
