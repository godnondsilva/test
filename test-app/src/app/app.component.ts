import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ButtonLoaderService } from './button-loader.service';
import { ButtonComponent } from './button/button.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ButtonComponent, HttpClientModule],
  providers: [ButtonLoaderService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'test-app';

  http = inject(HttpClient);

  constructor(
    protected aloader: ButtonLoaderService,
    protected bloader: ButtonLoaderService
  ) {
    console.log(this.aloader.state);
    console.log(this.bloader.state);
  }

  ngOnInit() {
    this.bloader.setState('loading');
    this.http.get('http://demo7774410.mockable.io/').subscribe({
      next: (data) => {
        console.log(data);
        console.log('success');
        this.bloader.setState('success');
      },
      error: (err) => {
        console.log(err);
        console.log('error');
        this.bloader.setState('error');
      },
    });
  }
}
